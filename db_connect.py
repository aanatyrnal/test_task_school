import asyncpg
from settings import settings
from contextlib import asynccontextmanager


@asynccontextmanager
async def connect():
    try:
        conn = await asyncpg.connect(
            host=settings.POSTGRES_HOSTNAME,
            database=settings.POSTGRES_DATABASE,
            user=settings.POSTGRES_USER,
            password=settings.POSTGRES_PASSWORD.get_secret_value(),
            port=settings.POSTGRES_PORT)

        yield conn
        await conn.close()

    except Exception as ex:
        print(ex)
