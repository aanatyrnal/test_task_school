from pydantic import BaseModel


class User(BaseModel):
    id: int
    name: str
    gender: str


class CreateUser(BaseModel):
    name: str
    gender: str
