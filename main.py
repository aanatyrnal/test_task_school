from fastapi import FastAPI

import postgres
from model import *

app = FastAPI(prefix="/users", tags=["Users"])


@app.get("/read_all_user")
async def read_all_user():
    """ 1 Получение всех строк из бд """
    return await postgres.read_all_user()


@app.get("/read_user")
async def read_user(gender: str, limit: int):
    """
    2 Отдает данные из БД,
    с возможностью фильтрации по полу, с заданным LIMIT
    """
    return await postgres.read_user(gender=gender, limit=limit)


@app.put("/")
async def create_user(data: CreateUser):
    """
    3 Принимает JSON{id, name, gender}, парсит данные и сохраняет в бд.
    Необходимо валидировать данные (по типу).
    """
    return await postgres.create_user(data)
