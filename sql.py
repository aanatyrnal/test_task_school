"""
create table "user" (
    id      serial PRIMARY key,
    name    VARCHAR(20) not NULL,
    gender  VARCHAR(20) not NULL
    ) ;
"""

"""
INSERT INTO "user" (name, gender) 
VALUES ('AA', 'F') ;
INSERT INTO "user" (name, gender) 
VALUES ('BB', 'M') ;
INSERT INTO "user" (name, gender) 
VALUES ('CC', 'F') ;
INSERT INTO "user" (name, gender) 
VALUES ('DD', 'M') ;
"""
