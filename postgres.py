from settings import settings
import db_connect
from model import *


async def read_all_user():
    async with db_connect.connect() as conn:
        user = await conn.fetch(
            """
            SELECT id, "name", gender 
            from "user" ;
            """
        )
        return user


async def read_user(gender: str, limit=3):
    async with db_connect.connect() as conn:
        user = await conn.fetch(
            """
            SELECT id, "name", gender 
            from "user" 
            WHERE gender = $1 
            LIMIT $2 ;
            """,
            gender,
            limit
        )
        return user


async def create_user(data: CreateUser):
    async with db_connect.connect() as conn:
        user = await conn.fetch(
            """
            INSERT INTO "user" (name, gender) 
            VALUES ($1, $2) 
            returning id, "name", gender ;
            """,
            data.name,
            data.gender
        )
        return user
