FROM python:3.10

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app/

RUN pip install --upgrade pip

COPY ./req.txt .
RUN pip install -r req.txt


COPY . /usr/src/app/

CMD ["poetry", "run", "uvicorn", "main:app", "--host", "0.0.0.0", "--port", "5555"]
